package com.vikings.kiddykingdom;

import android.app.Activity;
import android.content.Intent;
import android.graphics.drawable.AnimationDrawable;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

public class colormenu extends Activity implements OnClickListener 
{

	AnimationDrawable frameAnimation;
	ImageView view;
	@Override
	protected void onCreate(Bundle savedInstanceState) 
	{
		super.onCreate(savedInstanceState);

		overridePendingTransition(R.anim.anim_in, R.anim.anim_out);
		setContentView(R.layout.colormenu);
		view = (ImageView) findViewById(R.id.imageAnimation);
		view.setBackgroundResource(R.drawable.coloranimation);

		// Typecasting the Animation Drawable
		frameAnimation = (AnimationDrawable) view.getBackground();
		
		frameAnimation.start();
		
	}
	
	@Override
	public void onClick(View v) 
	{
		
	}
	
	@Override 
	public void onBackPressed()
	{
		finish();
		Intent previous = new Intent(colormenu.this, MainActivity.class);
		previous.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(previous);
	
	}
	
}
