package com.vikings.kiddykingdom;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.TextView;

public class alpha12 extends Activity 
{
	protected void onCreate(Bundle savedInstanceState) 
	{
		super.onCreate(savedInstanceState);

		overridePendingTransition(R.anim.anim_in, R.anim.anim_out);
		setContentView(R.layout.alpha12);
		ImageView left =(ImageView) findViewById(R.id.leftarrow);
		ImageView right = (ImageView) findViewById (R.id.rightarrow);
		
		Typeface type = Typeface.createFromAsset(getAssets(),"fonts/comic.ttf"); 
		TextView ans1 = (TextView) findViewById (R.id.ans1);
		ans1.setTypeface(type);
		TextView ans2 = (TextView) findViewById (R.id.ans2);
		ans2.setTypeface(type);
		TextView ans3 = (TextView) findViewById (R.id.ans3);
		ans3.setTypeface(type);
		
		
		left.setOnClickListener(new OnClickListener()
		{            
	      public void onClick(View v) 
	      {
	    	  startActivity(new Intent(getApplicationContext(), alpha11.class));               
	     }
		});
		
		right.setOnClickListener(new OnClickListener()
		{            
		      public void onClick(View v) 
		      {
		    	  startActivity(new Intent(getApplicationContext(), alpha13.class));               
		     }
			});
		
		
	}
	
	@Override 
	public void onBackPressed()
	{
		finish();
		Intent previous = new Intent(alpha12.this, alphabet.class);
		previous.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(previous);
	
	}
}
