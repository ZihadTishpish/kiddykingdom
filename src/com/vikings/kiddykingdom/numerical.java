package com.vikings.kiddykingdom;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

public class numerical extends Activity implements OnClickListener 
{
	Button tap;
	ImageView rightarrow,leftarrow;
	int count = 1;
	TextView Digit, spelling;
	String name[]= {"One","Two","Three","Four","Five","Six","Seven","Eight","Nine","Ten","Eleven","Twelve","Thirteen","fourteen","Fifteen","Sixteen","Seventeen","Eighteen","Nineteen"};
	String firstname[] = {"Zero","Ten","Twenty","Thirty","Forty","Fifty","Sixty","Seventy","Eighty","Ninety","One Hundred"};
	String dig[]={"0","1","2","3","4","5","6","7","8","9"};
	Animation slideright;
	@Override
	protected void onCreate(Bundle savedInstanceState) 
	{
		super.onCreate(savedInstanceState);

		overridePendingTransition(R.anim.anim_in, R.anim.anim_out);
		setContentView(R.layout.numerical);
		slideright =   AnimationUtils.loadAnimation(getApplicationContext(),R.anim.slidingformenu);

		Digit  = (TextView) findViewById (R.id.digit);
		spelling  = (TextView) findViewById (R.id.spelling);
		Digit.setText(dig[count]);
		spelling.setText(name[count-1]);
		 rightarrow = (ImageView) findViewById(R.id.rightarrow);
		rightarrow.setOnClickListener(this);
		leftarrow = (ImageView) findViewById(R.id.leftarrow);
		leftarrow.setOnClickListener(this);

		Digit.startAnimation(slideright);
		
	}
	@Override
	public void onBackPressed()
	{
		finish();
		Intent previous = new Intent(this, MainActivity.class);
		previous.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(previous);
			
	}
	
	
	@Override
	public void onClick(View v) 
	{
		int id = v.getId();
		if (id == R.id.rightarrow)
		{
			count = count+1;
			change();
		}
		
		else if (id == R.id.leftarrow)
		{
			count = count-1;
			change();
		}
		
	}

	private void change()
	{
		// TODO Auto-generated method stub
		
		if (count<10 && count >0)
		{
		Digit.setText(dig[count]);
		spelling.setText(name[count-1]);
		Digit.startAnimation(slideright);
		}
		else if (count%10==0 && count!=100 && count !=0)
		{
			Digit.setText(dig[count/10]+"0");
			spelling.setText(firstname[count/10]);
			Digit.startAnimation(slideright);
		}
		else if (count == 100 )
		{
			Digit.setText("100");
			spelling.setText("One Hundred");
			Digit.startAnimation(slideright);
		}
		else if (count > 100 )
		{
			count =1;
			Digit.setText("1");
			spelling.setText("One");
			Digit.startAnimation(slideright);
		}
		else if (count <=0)
		{
			count = 0;
		}
		
		else
		{
			if (count/10==1)
			{

				Digit.setText(dig[count/10]+dig[count%10]);
				spelling.setText(name[count-1]);

				slideright =   AnimationUtils.loadAnimation(getApplicationContext(),R.anim.slidingformenu);
				Digit.startAnimation(slideright);
			}
			else
			{
			Digit.setText(dig[count/10]+dig[count%10]);
			spelling.setText(firstname[count/10]+" "+ name[count%10-1]);

			slideright =   AnimationUtils.loadAnimation(getApplicationContext(),R.anim.slidingformenu);
			Digit.startAnimation(slideright);
			}
		}
		
	}
}
